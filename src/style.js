const style = {
    commentBox: {
        width: '80vw',
        margin: '0 auto',
        fontFamily: 'Helvetica, sans-serif',
    },
    title: {
        textAlign: 'center',
        textTransform: 'uppercase'
    },
    commentList: {
        border: '1px solid #f1f1f1',
        padding: '0 12px',
        maxHeight: '70vh',
        overflow: 'scroll'
    },
    comment: {
        border: '1px solid #e5e5e5',
        margin: '10px',
        padding: '3px 10px',
        fontSize: '.85rem',
        borderRadius: '5px'
    },
    commentForm: {
        margin: '10px',
        display: 'flex',
        flexFlow: 'row wrap',
        justifyContent: 'space-between'
    },
    commentFormAuthor: {
        minWidth: '150px',
        margin: '3px',
        padding: '0 10px',
        borderRadius: '3px',
        height: '40px',
        flex: '2'
    },
    commentFormText: {
        flex: '4',
        minWidth: '400px',
        margin: '3px',
        padding: '0 10px',
        height: '40px',
        borderRadius: '3px'
    },
    commentFormPost: {
        minWidth: '75px',
        flex: '1',
        height: '40px',
        margin: '5px 3px',
        fontSize: '1rem',
        backgroundColor: '#A3CDFD',
        borderRadius: '3px',
        color: '#fff',
        textTransform: 'uppercase',
        letterSpacing: '.055rem',
        border: 'none'
    },
    commentFormPostCancel: {
        minWidth: '75px',
        flex: '1',
        height: '40px',
        margin: '5px 3px',
        fontSize: '1rem',
        backgroundColor: 'red',
        borderRadius: '3px',
        color: '#fff',
        textTransform: 'uppercase',
        letterSpacing: '.055rem',
        lineHeight: '4px',
        border: 'none'
    },
    updateLink: {
        textDecoration: 'none',
        paddingRight: '15px',
        fontSize: '16px',
    },
    deleteLink: {
        color: 'red',
        textDecoration: 'none',
        paddingRight: '15px',
        fontSize: '16px',
        marginTop: '20px'
    },
    replyLink: {
        textDecoration: 'none',
        paddingRight: '15px',
        fontSize: '16px',
        color: "#ccc",
        marginTop: '20px'
    },
    flex: {
        display: 'flex',
        justifyContent: 'space-between',
        borderBottom: '1px solid #ccc'
    }
}
module.exports = style;