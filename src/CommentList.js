//CommentList.js
import React, {Component} from 'react';
import Comment from './Comment';
import style from './style';

class CommentList extends Component {
    constructor(props) {
        super(props);
        this.renderSubMenu = this.renderSubMenu.bind(this);
        this.newArray = [];
    }

    renderSubMenu(options) {

        const menuOptions = options.map(comment => {
            const display = (
                    <Comment
                        comment={comment}
                        uniqueID={comment['_id']}
                        onCommentDelete={this.props.onCommentDelete}
                        onCommentUpdate={this.props.onCommentUpdate}
                        onCommentReply={this.props.onCommentReply}
                        key={comment['_id']}>
                        {comment.text}
                    </Comment>
                ),
                hasSub = (comment.sub && comment.sub.length > 0);
            let subMenu;
            if (hasSub) {
                subMenu = this.renderSubMenu(comment.sub);
            }
            return (
                <li key={comment['_id']}>
                    {display}
                    {subMenu}
                </li>
            );
        });
        return (
            <div className='dropdown__options'>
                <ul>
                    {menuOptions}
                </ul>
            </div>
        );
    }

    render() {
        return (
            <div style={style.commentList}>
                {this.renderSubMenu(this.props.data)}
            </div>
        )
    }

}

export default CommentList;
