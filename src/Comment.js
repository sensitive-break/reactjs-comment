//Comment.js
import React, {Component} from 'react';
import 'font-awesome/css/font-awesome.min.css';
import {nl2br, date} from './Helpers';
import marked from 'marked';
import style from './style';

class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            update: {
                toBeUpdated: false,
                author: this.props.comment.author,
                date: this.props.comment.date,
                text: this.props.comment.text,
                replyId: this.props.comment.replyId
            },
            reply: {
                toBeReply: false,
                author: '',
                date: '',
                text: '',
                replyId: null
            }
        };
        this.type = "update";
        //binding all our functions to this class
        this.date = date;
        this.nl2br = nl2br;
        this.deleteComment = this.deleteComment.bind(this);
        this.updateComment = this.updateComment.bind(this);
        this.updateReply = this.updateReply.bind(this);
        this.handleAuthorChange = this.handleAuthorChange.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleCommentUpdateOrReply = this.handleCommentUpdateOrReply.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    updateComment(e) {
        e.preventDefault();
        this.type = "update";
        //brings up the update field when we click on the update link.

        this.setState({ update: {...this.state.update, toBeUpdated: true} });
    }

    cancel() {
        if(this.type === "update") {
            this.setState({ update: {...this.state.update, toBeUpdated: false} });
        }
        if(this.type === "reply") {
            this.setState({ reply: {...this.state.reply, toBeReply: false} });
        }
    }

    updateReply(e) {
        e.preventDefault();
        this.type = "reply";
        //brings up the update field when we click on the update link.
        this.setState({ reply: {...this.state.reply,
                toBeReply: true,
                author: '',
                date: '',
                text: '',
                replyId: null}
        });
    }

    handleCommentUpdateOrReply(e) {
        e.preventDefault();
        let id = this.props.uniqueID;
        //if author or text changed, set it. if not, leave null and our PUT 
        //request will ignore it.
        let author = (this.state[this.type].author) ? this.state[this.type].author : null;
        let text = (this.state[this.type].text) ? this.state[this.type].text : null;
        let replyId = ( this.props.comment._id) ? this.props.comment._id : null;
        let comment = {author: author, text: text, replyId: replyId};
        if (this.state[this.type].toBeReply) {
            this.props.onCommentReply(comment);
            this.setState({ reply: { ...this.state.reply, toBeReply:  !this.state[this.type].toBeReply}});
        } else {
            this.props.onCommentUpdate(id, comment);
            this.setState({ update: { ...this.state.update, toBeUpdated: !this.state[this.type].toBeUpdated}});
        }
    }

    deleteComment(e) {
        e.preventDefault();
        let id = this.props.uniqueID;
        this.props.onCommentDelete(id);
        console.log('oops deleted');
    }

    handleTextChange(e) {
        this.setState({ [this.type]: {...this.state[this.type], text: e.target.value}});
    }

    handleAuthorChange(e) {
        this.setState({ [this.type]: {...this.state[this.type], author: e.target.value}});
    }

    rawMarkup() {
        let rawMarkup = marked(this.nl2br(this.state.update.text.toString()));
        return {__html: rawMarkup};
    }

    static buttonMarkup(text) {
        let rawMarkup = marked(text.toString());
        return {__html: rawMarkup};
    }

    render() {
        return (
            <div style={style.comment} className={this.props.class}>
                <div style={style.flex}>
                    <div>
                        <h3>{this.state.update.author}</h3>
                        <i dangerouslySetInnerHTML={this.date(this.state.update.date)}/>
                    </div>
                    <div>
                        <a style={style.updateLink} href='#' onClick={this.updateComment} className="fa fa-pencil-square-o"/>
                        <a style={style.replyLink} href='#' onClick={this.updateReply} className="fa fa-reply"/>
                        <a style={style.deleteLink} href='#' onClick={this.deleteComment}  className="fa fa-remove"/>
                    </div>
                </div>
                <span dangerouslySetInnerHTML={this.rawMarkup()}/>

                {(this.state[this.type].toBeUpdated || this.state[this.type].toBeReply)
                    ? (<form onSubmit={this.handleCommentUpdateOrReply}>
                        <input
                            type='text'
                            placeholder='Name'
                            style={style.commentFormAuthor}
                            value={this.state[this.type].author}
                            onChange={this.handleAuthorChange}/>
                        <textarea
                            type='text'
                            placeholder='Your comment'
                            style={style.commentFormText}
                            value={this.state[this.type].text}
                            onChange={this.handleTextChange}/>
                        <input
                            type='submit'
                            style={style.commentFormPost}
                            value={this.type}/>
                        <button style={style.commentFormPostCancel} onClick={this.cancel} dangerouslySetInnerHTML={Comment.buttonMarkup("Cancel")}/>
                    </form>)
                    : null}
            </div>
        )
    }
}

export default Comment;